$(document).ready(() => {
  const buttonElem = $("#menuToggle"),
    targetElem = $(buttonElem.attr("data-target"));

  buttonElem.on("click", () => {
    targetElem.slideToggle(200);
  });

  $(window).on("resize", () => {
    if (window.innerWidth >= 768) {
      targetElem.attr("style", "");
    }
  });
});
