/*## Задание 1
1. Дан массив городов `["Kyiv", "Berlin", "Dubai", Madrid", "Paris"]`;
2. Выведите в консоль все свойства этого массива с помощью синтаксиса десктруктуризации.

## Задание 2
Создайте обьект Employee. Запишите в него свойства name, salary и присвойте любые значение.
С помощью синтаксиса десктруктуризации сделайте так, чтоб запись console.log(name, salary)
выводила в консоль - значения этих свойств в обьекте Employee;

## Задание 3
Дополните код так, чтоб он был рабочим
const array = ['value', 'showValue']

alert(value); // будет выведено 'value'
alert(showValue);  // будет выведено 'showValue'*/

//Задание 1

const cities = ["Kyiv", "Berlin", "Dubai", "Madrid", "Paris"];

const showCities = ([ukrcity, germcity, uaecity, spacity, francity]) => {
    console.log(`
    Ukraine: ${ukrcity},
    Germany: ${germcity},
    UAE: ${uaecity},
    Spain: ${spacity},
    France: ${francity}
    `);
}

showCities(cities);

//Задание 2

const Employee = {
    name: "Ivan Petrov",
    salary: 1500
}

const { name, salary } = Employee;

console.log(name, salary);

//Задание 3

const array = ['value', 'showValue'];

const [value, showValue] = array;

alert(value);
alert(showValue);