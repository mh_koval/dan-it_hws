/*Задание
Дан массив books.
    const books = [
        {
          author: "Скотт Бэккер",
          name: "Тьма, что приходит прежде",
          price: 70
        },
        {
         author: "Скотт Бэккер",
         name: "Воин-пророк",
        },
        {
          name: "Тысячекратная мысль",
          price: 70
        },
        {
          author: "Скотт Бэккер",
          name: "Нечестивый Консульт",
          price: 70
        },
        {
         author: "Дарья Донцова",
         name: "Детектив на диете",
         price: 40
        },
        {
         author: "Дарья Донцова",
         name: "Дед Снегур и Морозочка",
        }
     ];

- Выведите этот массив в виде списка (тег ul).
- На странице должен находится div с id="root", куда и нужно будет положить этот список.
- Перед выводом обьекта на странице, нужно проверить его на корректность
(в обьекте должны содержаться все три свойства - author, name, price).
Если какого-то из этих свойств нету, в консоли должна высветится ошибка с указанием - какого свойства нету в обьекте.
- Тот обьект, что некорректен по условиям предыдущего пункта - не должен появиться на странице.*/

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const targetElem = document.querySelector("#root");
const listWrap = document.createElement("ul");
targetElem.append(listWrap)

const createList = ({author, name, price}) => {
    const listItem = document.createElement("li");
    listItem.innerHTML = `<ul><li>Автор: <b>${author}</b></li><li>Название: <i>${name}</i></li><li>Цена: ${price}</li></ul>`;
    listWrap.append(listItem);
}

const showList = (arr) => {
    arr.forEach(item => {
        try {
            if (!item.author) {
                throw new Error("Не указан автор");
            }
            if (!item.name) {
                throw new Error("Без названия");
            }
            if (!item.price) {
                throw new Error("Нет цены");
            }

            createList({
                author: item.author,
                name: item.name,
                price: item.price
            });

        } catch (error) {
            console.log(item)
            console.error(error);
        }
    })
}

showList(books);