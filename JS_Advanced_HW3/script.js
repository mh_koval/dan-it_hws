/*Задание
1. Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary
(зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.
2. Сделайте геттеры и сеттеры для этих свойств.
3. Сделайте класс Programmer, который будет наследоваться от класса Employee, у которого будет свойство lang
(список языков)
4. Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary
умноженное на 3.
5. Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.*/

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
    get age() {
        return this._age;
    }
    set age(value) {
        this._age = value;
    }
    get salary() {
        return this._salary;
    }
    set salary(value) {
        this._salary = value;
    }

}

class Programmer extends Employee {
    constructor(lang, ...rest) {
        super(...rest);
        this.lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }

    set salary(value) {
        super.salary = value;
    }

}

let employee = new Employee("Andrey", 25, 1500);
let programmer1 = new Programmer(["Go", "Kotlin", "Rust"], "Sergey", 30, 5000);
let programmer2 = new Programmer(["Java", "Python"], "Petr", 23, 2500);
let programmer3 = new Programmer(["C++", "PHP", "Ruby"], "Viktor", 35, 4500);
let programmer4 = new Programmer(["Objective-C", "Swift", "Dart"], "Aleksandr", 28, 3000);

console.log( employee, programmer1, programmer2, programmer3, programmer4 );
