/*Задание

Реализовать простую программу на Javascript, которая будет взаимодействовать с пользователем с помощью
модальных окон браузера - `alert`, `prompt`, `confirm`. Задача должна быть реализована на языке javascript,
без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
- Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
- Если возраст меньше 18 лет - показать на экране сообщение: `You are not allowed to visit this website`.
- Если возраст от 18 до 22 лет (включительно) - показать окно со следующим сообщением: `Are you sure you
want to continue?` и кнопками `Ok`, `Cancel`. Если пользователь нажал `Ok`, показать на экране сообщение:
`Welcome, ` + имя пользователя. Если пользователь нажал `Cancel`, показать на экране сообщение: `You are not
allowed to visit this website`.
- Если возраст больше 22 лет - показать на экране сообщение: `Welcome, ` + имя пользователя.
- Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.

Необязательное задание продвинутой сложности:
- После ввода данных добавить проверку их корректности. Если пользователь не ввел имя, либо при вводе
возраста указал не число - спросить имя и возраст заново (при этом значением по умолчанию для каждой из
переменных должна быть введенная ранее информация).*/

let age = +prompt("How old are you?");
let name = prompt("What's your name?") || "";

if (name === "" || isNaN(age) === true) {
    age = +prompt("How old are you?", age) || "";
    name = prompt("What's your name?", name) || "";
}
if (age < 18) {
    alert("You are not allowed to visit this website");
} else if (age >= 18 && age<= 22) {
    let ageConfirmation = confirm("Are you sure you want to continue?");
    if (ageConfirmation === true)  {
        alert("Welcome, "+name);
    } else if (ageConfirmation === false) {
        alert("You are not allowed to visit this website");
    }
} else if (age > 22) {
    alert("Welcome, "+name);
}

