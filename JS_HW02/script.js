/*Задание

Реализовать программу на Javascript, которая будет находить все числа кратные 5 (делятся на 5 без остатка) в
заданном диапазоне. Задача должна быть реализована на языке javascript, без использования фреймворков и
сторонник библиотек (типа Jquery).

Технические требования:
- Считать с помощью модального окна браузера число, которое введет пользователь.
- Вывести в консоли все числа кратные 5, от 0 до введенного пользователем числа. Если таких чисел нету -
вывести в консоль фразу `Sorry, no numbers'
- Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.

Необязательное задание продвинутой сложности:
- Проверить, что введенное значение является целым числом. Если данное условие не соблюдается, повторять
вывод окна на экран до тех пор, пока не будет введено целое число.
- Считать два числа, `m` и `n`. Вывести в консоль все простые числа (http://ru.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE) в диапазоне от `m` до `n` (меньшее из введенных чисел будет `m`, бОльшее будет `n`). Если хотя бы одно из чисел не соблюдает
условие валидации, указанное выше, вывести сообщение об ошибке, и спросить оба числа заново.*/

let maxRangeNumber = +prompt("Enter your number");

while (isNaN(maxRangeNumber)) {
    maxRangeNumber = +prompt("Enter your number again");
}

while (maxRangeNumber % 1 !== 0) {
    maxRangeNumber = +prompt("Enter integer number");
}

for (let range = 0; range <= maxRangeNumber; range += 5) {
    if (maxRangeNumber < 5) {
        console.log ("Sorry, no numbers");
    } else {
        console.log (range);
    }
}
//доп.задание
alert("Start advanced task");
console.clear();

let m = +prompt("Your first number") || "";
let n = +prompt("Your second number") || "";

while (m >= n) {
    m = +prompt("Eror! Need less first number than second");
    n = +prompt("Your second number");
}

primeNumber: for (; m <= n; m++) {
        if (m <= 1) {
            continue;
        }
        if (m === 2) {
            console.log(m);
            continue;
        }
        for (let count = 2; count < m; count++) {
            if (m % count === 0) {
                continue primeNumber;
            }
        }
        console.log(m);
    }

