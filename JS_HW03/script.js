/*Задание

Реализовать функцию, которая будет производить математические операции с введеными пользователем числами.
Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек
(типа Jquery).

Технические требования:
- Считать с помощью модального окна браузера два числа.
- Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может
быть введено `+`, `-`, `*`, `/`.
- Создать функцию, в которую передать два значения и операцию.
- Вывести в консоль результат выполнения функции.

Необязательное задание продвинутой сложности:
- После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе
указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных
должна быть введенная ранее информация).*/

let numFirst = checkNumber(prompt("Введите первое число"));
let numSecond = checkNumber(prompt("Введите второе число"));
let operator = prompt("Введите знак операции");
 
function checkNumber(numData) {
		let previous = numData;
    if (!isNaN(+numData)) {
      return +numData;
    } else {
      for (; ;) { 
        if (isNaN(+numData)) {
        numData = prompt("Введите еще раз значение, это должно быть число", previous);
} else {
	return +numData;
			}
		}
	}
}

function calcResult(numFirst, numSecond, operator) {
  return window.eval(`${numFirst}` + operator + `${numSecond}`);// знаю, что eval is evil, но switch - это так банально, все так сделали))
  /*switch (operator) {
    case "+": return numFirst + numSecond;
    case "-": return numFirst - numSecond;
    case "*": return numFirst * numSecond;
    case "/": return numFirst / numSecond;
  }*/
}

console.log(calcResult(numFirst, numSecond, operator));