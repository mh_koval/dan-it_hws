/*Задание

Дополнить функцию `createNewUser()` c методами подсчета возраста пользователя и его паролем. Задача должна
быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
- Возьмите выполненное домашнее задание номер 4 (созданная вами функция `createNewUser()`) и дополните ее
следующим функционалом:
   1. При вызове функция должна спросить у вызывающего дату рождения (текст в формате `dd.mm.yyyy`) и
   сохранить ее в поле `birthday`.
   2. Создать метод `getAge()` который будет возвращать сколько пользователю лет.
   3. Создать метод `getPassword()`, который будет возвращать первую букву имени пользователя в верхнем
   регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, `Ivan Kravchenko
   13.03.1992 → Ikravchenko1992`).
- Вывести в консоль результат работы функции `createNewUser()`, а также функций `getAge()` и `getPassword()`
созданного объекта.*/

function createNewUser() {

    let newUser = {
        firstName: prompt("Введите имя") || "",
        lastName: prompt("Введите фамилию") || "",
        birthday: prompt("Введите дату рождения (dd.mm.yyyy)") || "",
        getLogin() {
            return `${this.firstName.toLowerCase().substring(0,1)}${this.lastName.toLowerCase()}`;
        },
        getDateOfBirth() {
            return new Date(this.birthday.split(".").reverse().join("-"));
        },
        getAge() {
            let birthday = this.getDateOfBirth();
            let nowDate = new Date();
            let age = nowDate.getFullYear() - birthday.getFullYear();
            return (nowDate.setFullYear(2000) - birthday.setFullYear(2000)) > 1 ? age : age - 1;
        },
        getPassword () {
            return `${this.firstName.substring(0,1).toUpperCase()}${this.lastName.toLowerCase()}${this.getDateOfBirth().getFullYear()}`;
        },
    }
    return newUser;
}


let newUser = createNewUser();
console.log(newUser.getAge());
console.log(newUser.getPassword());