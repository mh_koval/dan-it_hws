/* - Написать функцию `filterBy()`, которая будет принимать в себя 2 аргумента. Первый 
аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип  данных.
- Функция должна вернуть новый массив, который будет содержать в себе все данные, которые 
были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. То 
есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 
'string', то функция вернет массив [23, null]. */

let arr = ['hello', 'world', 23, '23', null];

function filterBy(arr, type) {
    let res = [];

    arr.forEach(item => {if (typeof item !== type) res.push(item)})
    //arr.filter(item => {if (typeof item !== type) res.push(item);})
    //тоже работает
    return res;
}

console.log(filterBy(arr, "string"));