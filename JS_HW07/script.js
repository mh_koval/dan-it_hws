/*Задание

Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка.
Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек
(типа Jquery).

Технические требования:
- Создать функцию, которая будет принимать на вход массив.
- Каждый из элементов массива вывести на страницу в виде пункта списка
- Необходимо использовать шаблонные строки и функцию `map` массива для формирования контента списка
перед выведением его на страницу.
- Примеры массивов, которые можно выводить на экран:
   ```javascript
   ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
   ```

   ```javascript
   ['1', '2', '3', 'sea', 'user', 23]
   ```
- Можно взять любой другой массив.

#### Необязательное задание продвинутой сложности:
- ??? Очистить страницу через 10 секунд. Показывать таймер обратного отсчета (только секунды)
перед очищением страницы.
- Если внутри массива одним из элементов будет еще один массив или объект, выводить его как вложенный список.*/

let arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv', ["a", "b", [100, 200, 300], "c","d"],
    true, 1, 2, -3.5, NaN, undefined, null];

function createListContent(arr) {
	let elemsList = document.createElement("ul");

    let res = arr.map(elem => {
        if (Array.isArray(elem)) {
            return listItem = createListContent(elem);
        } else {
            return `<li>${elem}</li>`;
        }
    })

    return res;
}

function createListOnPage(arr) {
    let elemsList = document.createElement("ul");

    arr.forEach(elem => {
        if (Array.isArray(elem)) {
            let listItem = document.createElement("li");
            listItem.insertAdjacentElement("beforeend", createListOnPage(elem));
            elemsList.insertAdjacentElement("beforeend", listItem);
        } else {
            elemsList.insertAdjacentHTML("beforeend", elem);
        }

    });

    return elemsList;
}

const onPage = (data => {
    document.querySelector("body").append(data);
})

onPage(createListOnPage(createListContent(arr)));