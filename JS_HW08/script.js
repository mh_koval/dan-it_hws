/*Задание

Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript,
без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
- При загрузке страницы показать пользователю поле ввода (`input`) с надписью `Price`.
Это поле будет служить для ввода числовых значений
- Поведение поля должно быть следующим:
   - При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
   - Когда убран фокус с поля - его значение считывается, над полем создается `span`, в котором должен быть выведен текст:
   `Текущая цена: ${значение из поля ввода}`. Рядом с ним должна быть кнопка с крестиком (`X`).
   Значение внутри поля ввода окрашивается в зеленый цвет.
   - При нажатии на `Х` - `span` с текстом и кнопка `X` должны быть удалены.
   Значение, введенное в поле ввода, обнуляется.
   - Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой,
   под полем выводить фразу - `Please enter correct price`. `span` со значением при этом не создается.
- В папке `img` лежат примеры реализации поля ввода и создающегося `span`.*/

const getPrice = document.querySelector(".input");
const priceInfo = document.querySelector(".container");

const createPriceBlock = inputValue => {
    const priceBlock = document.createElement("span");
    priceBlock.innerText = `Текущая цена: ${inputValue}$`;

    const crossButton = document.createElement("button");
    crossButton.classList.add("close-price-text");
    crossButton.innerHTML = "&#10008;";

    priceBlock.append(crossButton);
    crossButton.addEventListener("click", closePriceBlock);

    return priceBlock;
}

function closePriceBlock() {
    this.parentElement.remove();
    getPrice.value = "";
}

const showErrorInfo = () => {
    const errorText = document.createElement("span");
    errorText.classList.add("error-text");
    errorText.innerHTML = "Please enter correct price";

    return errorText;
}

getPrice.onfocus = event => {
    event.target.classList.add("onfocus-border");
    event.target.classList.remove("error-border");
    getPrice.value = "";
    event.target.parentElement.nextSibling.remove();
}

getPrice.onblur = event => {
    event.target.classList.remove("onfocus-border");

   let priceValue = event.target.value;
    if (priceValue !== "" && +priceValue >= 0) {
        event.target.classList.add("input-text");
        event.target.classList.remove("onfocus-border");
        priceInfo.append(createPriceBlock(priceValue));
    } else if (priceValue === null) {
        event.target.classList.remove("onfocus-border");
    } else if (+priceValue < 0) {
        event.target.classList.add("error-border");
        event.target.classList.remove("input-text");
        event.target.parentElement.after(showErrorInfo());
    }
}

