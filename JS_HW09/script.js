/*Задание

Реализовать переключение вкладок (табы) на чистом Javascript.

Технические требования:
- В папке `tabs` лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст
для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен
отображаться для какой вкладки.
- Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
- Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться.
При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.*/

window.addEventListener("load", () => {

    const allTabs = document.querySelectorAll(".tabs-title");
    const tabItem = document.querySelectorAll(".tab-content");
    let currentTab = document.querySelector(".active");
    let currentContent = document.querySelector(".current-content");

    allTabs.forEach((elem) => {
        elem.addEventListener("click", () => {
            currentTab.classList.remove("active");
            elem.classList.add("active");
            currentTab = elem;
            currentContent.classList.remove("current-content");
            tabItem[elem.dataset.tabElem - 1].classList.add("current-content");
            currentContent = tabItem[elem.dataset.tabElem - 1];
        });
    });
});
