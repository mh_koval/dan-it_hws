/*Задание

Написать реализацию кнопки "Показать пароль". Задача должна быть реализована на языке javascript, без
использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
- В файле `index.html` лежит разметка для двух полей ввода пароля.
- По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь,
иконка меняет свой внешний вид.
- Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
- Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
- По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
- Если значения совпадают - вывести модальное окно (можно alert) с текстом - `You are welcome`;
- Если значение не совпадают - вывести под вторым полем текст красного цвета  `Нужно ввести одинаковые
значения`
- После нажатия на кнопку страница не должна перезагружаться
- Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.
*/

window.addEventListener("DOMContentLoaded", () => {

    let enterPassword = document.querySelector('.enter-password');
    let submitPassword = document.querySelector('.submit-password');
    let submitButton = document.querySelector('.btn');
    let iconStyle = document.querySelectorAll('.input-wrapper .icon-password');
    let errorMessage = document.querySelector('.error-message');

    iconStyle.forEach(elem => {
        elem.addEventListener('click', () => {

            toggleClasses(elem, 'fa-eye fa-eye-slash');
            let inputField = elem.closest('.input-wrapper').firstElementChild;
            toggleType(inputField);
        })
    })

    submitButton.addEventListener('click', (event) => {
        event.preventDefault();
        if (enterPassword.value === submitPassword.value) {
            removeError()
            alert('You are welcome');
        } else {
            errorMessage.classList.add('error-info');
        }
    })

    function removeError() {
        errorMessage.classList.remove('error-info');
    }

    function toggleClasses(elem, classes) {
        for (let classItem of classes.split(' ')) {
            elem.classList.toggle(classItem);
        }
        return this;
    }

    let toggleType = elem => {
        if (elem.type === 'password') {
            elem.type = 'text'
        } else {
            elem.type = 'password';
        }
    }
})
