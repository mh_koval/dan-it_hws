/*Задание

Реализовать программу, показывающую циклично разные картинки. Задача должна быть реализована на языке
javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

#### Технические требования:
- В папке `banners` лежит HTML код и папка с картинками.
- При запуске программы на экране должна отображаться первая картинка.
- Через 10 секунд вместо нее должна быть показана вторая картинка.
- Еще через 10 секунд - третья.
- Еще через 10 секунд - четвертая.
- После того, как покажутся все картинки - этот цикл должен начаться заново.
- При запуске программы где-то на экране должна появиться кнопка с надписью `Прекратить`.
- По нажатию на кнопку `Прекратить` цикл завершается, на экране остается показанной та картинка, которая
была там при нажатии кнопки.
- Рядом с кнопкой `Прекратить` должна быть кнопка `Возобновить показ`, при нажатии которой цикл
продолжается с той картинки, которая в данный момент показана на экране.
- Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.

#### Необязательное задание продвинутой сложности:
- При запуске программы на экране должен быть таймер с секундами и миллисекундами, показывающий сколько
осталось до показа следующей картинки.*/

window.addEventListener("DOMContentLoaded", () => {

    const createButtonContainer = () => {

        const container = document.createElement("div");
        container.classList.add("button-container");
        container.append(createPause(), createResume());

        bannerContainer.after(container);

        return container;
    }

    const createPause = () => {

        const pause = document.createElement("button");
        pause.classList.add("control-button", "pause");
        pause.innerHTML = "Прекратить";

        return pause;
    }

    const createResume = () => {

        const resume = document.createElement("button");
        resume.classList.add("control-button", "resume");
        resume.innerHTML = "Возобновить показ";

        return resume;
    }

    const createTimer = () => {
        const timerCont = document.createElement("div");
        timerCont.classList.add("timer-container");

        const seconds = document.createElement("div");
        seconds.classList.add("timer");
        const secondsElement = document.createElement("span");
        secondsElement.innerHTML = `${(timeRange - 1000) / 1000}`;
        secondsElement.setAttribute("id", "seconds");
        seconds.append(secondsElement);
        seconds.innerHTML += "c:";

        const milliseconds = document.createElement("div");
        milliseconds.classList.add("timer");
        const millisecondsElement = document.createElement("span");
        millisecondsElement.innerHTML = timeRange;
        millisecondsElement.setAttribute("id", "milliseconds");
        milliseconds.append(millisecondsElement);
        milliseconds.innerHTML += "млс";

        timerCont.append(seconds, milliseconds);

        document.querySelector(".images-wrapper").before(timerCont);
    }

    const showBanners = () => {
        if (document.querySelector(".paused")) {
            return;
        }

        if (timePoint === timeRange) {
            if (bannerNumber === banners.length) {
                bannerNumber = 0;
            }
            timePoint = 0;
            banners[bannerNumber].classList.add("current");
            previousBanner.classList.remove("current");
            previousBanner = banners[bannerNumber];
            bannerNumber++;

            elemSec.innerHTML = (timeRange - 1000) / 1000;
            elemMls.innerHTML = timeRange;
        } else if (timePoint % 1000 === 0 && timePoint) {
            --elemSec.innerHTML;
        }
        elemMls.innerHTML -= 4;

        timePoint += 4;
        mainTimer = setTimeout(showBanners, 4);
    }

    let timeRange = 10000;
    let banners = document.querySelectorAll(".image-to-show");
    let bannerContainer = document.querySelector(".images-wrapper");
    let buttonContainer = createButtonContainer();
    createTimer();
    let elemSec = document.querySelector("#seconds");
    let elemMls = document.querySelector("#milliseconds");
    let timePoint = 0;
    let bannerNumber = 1;
    let previousBanner = banners[0];
    let mainTimer = setTimeout(showBanners, 4);

    buttonContainer.onclick = event => {
        if (event.target.classList.contains("pause")) {
            event.target.classList.add("paused");
        }

        if (event.target.classList.contains("resume") && event.target.previousElementSibling.classList.contains("paused")) {
            event.target.previousElementSibling.classList.remove("paused");
            mainTimer = setTimeout(showBanners, 0);
        }
    }
})