/*Задание

Реализовать возможность смены цветовой темы сайта пользователем.
Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек
(типа Jquery).

#### Технические требования:
- Взять любое готовое домашнее задание по HTML/CSS.
- Добавить на макете кнопку "Сменить тему".
- При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение.
При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
- Выбранная тема должна сохраняться и после перезагрузки страницы*/

window.addEventListener("DOMContentLoaded", () => {

    const defaultMode = {
        'background-color': '#fff',
        'color-header': '#35444f',
        'ramda-header-1': 'inherit',
        'ramda-header-2': '#4bcaff',
        'color-footer': '#b4b7ba',
    }
    const darkMode = {
        'background-color': '#000',
        'color-header': '#844242',
        'ramda-header-1': '#9e4482',
        'ramda-header-2': '#7197d0',
        'color-footer': '#689268',
    }

    let currentMode;
    localStorage.getItem('mode') ? currentMode = darkMode : currentMode = defaultMode;

    const activateMode = () => {

        document.querySelector('body').style.backgroundColor = `${currentMode['background-color']}`;
        document.querySelector('.nav-header').style.backgroundColor = `${currentMode['color-header']}`;
        document.querySelector('.footer-flex').style.backgroundColor = `${currentMode['color-footer']}`;
        document.querySelector('.ramda-header a').style.color = `${currentMode['ramda-header-1']}`;
        document.querySelector('.ramda-header a span').style.color = `${currentMode['ramda-header-2']}`;
    }

    activateMode();

    const toggleButton = document.querySelector('#toggle-mode');
    toggleButton.addEventListener('click', toggleMode);

    function toggleMode() {

        if (currentMode === defaultMode) {
            currentMode = darkMode;
            localStorage.setItem('mode', 'dark');
        } else {
            currentMode = defaultMode;
            localStorage.removeItem('mode');
        }

        activateMode();
    }

})