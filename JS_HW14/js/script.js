/*Задание

Добавить в домашнее задание HTML/CSS №6 (Flex/Grid) различные эффекты с использованием jQuery

#### Технические требования:
- Добавить вверху страницы горизонтальное меню со ссылкой на все разделы страницы.
- При клике на ссылку - плавно прокручивать страницу вниз до выбранного места.
- Если прокрутить страницу больше чем на 1 экран вниз, справа внизу должна появляться кнопка "Наверх" с
фиксированным позиционариванием. При клике на нее - плавно прокрутить страницу в самый верх.
- Добавить под одной из секций кнопку, которая будет выполнять функцию `slideToggle()` (прятать и
показывать по клику) для данной секции.*/

$(document).ready(function () {

    $('.page-nav-item').click(function (event) {
        event.preventDefault();
        $(document.documentElement).animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 1000);
    });

    $('.scrolltop').click(function (event) {
        event.preventDefault();
        $(document.documentElement).animate({
            scrollTop: 0
        }, 1000);
    });

    $(document).scroll(function (event) {
        $(this).scrollTop() >= ($('.section').offset().top + $('.section').height()) ?
            $('.scrolltop').fadeIn('500') : $('.scrolltop').fadeOut('500');
    });

    $('.hide-show-btn').click(function () {

        const button = $(this);
        const section = button.closest('section').find('.container');

        section.slideToggle(300, function () {
            section.is(':hidden') ? button.text('Показать') : button.text('Скрыть');
        });
    })

});