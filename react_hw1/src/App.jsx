import React, {Component} from 'react';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import "./App.scss";


class App extends Component {

  state = {
    firstModalIsOpened: false,
    secondModalIsOpened: false,
  }
  
  showFirstModal = () => {
      const { firstModalIsOpened } = this.state;
      this.setState({firstModalIsOpened: !firstModalIsOpened});
  }

  showSecondModal = () => {
        const { secondModalIsOpened } = this.state;
        this.setState({secondModalIsOpened: !secondModalIsOpened});
    }

  hideModal = () => {
      this.setState(() => ({ firstModalIsOpened: false, secondModalIsOpened: false}));
  }

  updateData = (value) => {
    this.setState({ firstModalIsOpened: value,  secondModalIsOpened: value})
}
  
  render() {

    const { firstModalIsOpened, secondModalIsOpened } = this.state,
    firstClassPart = "first",
    secondClassPart = "second";

    return (
      <div className="App">

              <div className="btn_modal_wrapper">
                  <Button text="Open first modal" onClick={this.showFirstModal} backgroundColor="#B3382C" className="btn-modal_first"/>

                  <Button text="Open second modal" onClick={this.showSecondModal} backgroundColor="#36c024" className="btn-modal_second"/>
              </div>
            
              {firstModalIsOpened && <Modal classNamePart={firstClassPart}
                                            className="modal modal_first"
                                            header="Do you want to delete this file?"
                                            closeButton="close"
                                            updateData={this.updateData}
                                            text="Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"
                                            actions={[<Button text="Ok" className={`btn-modal_${firstClassPart}_submit`} onClick={this.hideModal} />,
                    <Button text="Cancel" className={`btn-modal_${firstClassPart}_cancel`} onClick={this.hideModal} />]} />}

              {secondModalIsOpened && <Modal classNamePart={secondClassPart}
                                             className="modal modal_second"
                                             header="Are you already leaving?"
                                             closeButton="close"
                                             updateData={this.updateData}
                                             text="We hope that you will come back soon. Many interesting suggestions will be prepared for you."
                                             actions={[<Button text="Submit" className={`btn-modal_${secondClassPart}_submit`} onClick={this.hideModal} />,
                    <Button text="Cancel" className={`btn-modal_${secondClassPart}_cancel`} onClick={this.hideModal} />]} />}
  </div>
    );
  };

}

export default App;

