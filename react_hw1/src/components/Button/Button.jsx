import React, {Component} from 'react';
import './Button.scss';

class Button extends Component {

    render() {
        const {text, backgroundColor, className, onClick} = this.props;

        return (
            <button onClick={onClick} style={{backgroundColor: backgroundColor}} className={className}>{text}</button>
        );

    }
}

export default Button;
