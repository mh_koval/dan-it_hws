import React, {Component} from 'react';
import './Modal.scss';

class Modal extends Component {
    state = {
        modal: true
    }

    hideByOutside = event => {
        const result = event.currentTarget === event.target ? this.props.updateData(!this.state.modal) && this.setState({ modal: false}) : null;
        return result;
        }

    render() {
        const {modal} = this.state;

        const { className, classNamePart, header, closeButton, text, actions } = this.props;

        return (
            <>
            {modal && <div className="modal_wrapper" onClick={this.hideByOutside}>
                <div className={`${className} popup-fade`}>
                    <header className={`modal_${classNamePart}_header`}>
                        <span>{header}</span><span className={closeButton} onClick={() => {
                            this.props.updateData(!this.state.modal) && this.setState({ modal: false})
                        }}></span>
                    </header>
                    <p className={`modal_${classNamePart}_text`}>{text}</p>
                    <div className={`modal_${classNamePart}_footer`}>
                        {actions}
                    </div>
                </div>
            </div>}
            </>
        );
    }
}

export default Modal;
