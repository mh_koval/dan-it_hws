import React, { PureComponent } from 'react';
import axios from 'axios';
import Goods from './components/Goods/Goods';
import './App.scss';

export default class App extends PureComponent {
  state ={
    goods: []
  }


  componentDidMount() {
    axios('/goods.json')
      .then(res => this.setState({goods: res.data}))
    }

  render() {

    return (
      <div className="App">
        <Goods goods={this.state.goods} />
      </div>
    )
  }

}

