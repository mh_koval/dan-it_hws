import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class Button extends PureComponent {

    render() {
        const {text, className, onClick} = this.props;

        return (
            <button onClick={onClick} className={className}>{text}</button>
        );

    }
}

Button.propTypes = {
	text: PropTypes.string.isRequired,
	className: PropTypes.string.isRequired,
	onClick: PropTypes.func.isRequired
}