import React, { PureComponent } from 'react';
import Product from './../Product/Product';
import PropTypes from 'prop-types';
import './Goods.scss'

export default class Goods extends PureComponent {

render() {
    
        const { goods } = this.props,
        products = goods.map((product) => (
            <Product key={product.sku} 
            productName={product.productName}
            path={product.path}
            price={product.price}
            sku={product.sku}
            color={product.color}
             />
        ))

        
        return (
            <div className="goods-wrapper">
                {products}
            </div>
        )
    }
}

Goods.propTypes = {
  goods: PropTypes.array.isRequired
}