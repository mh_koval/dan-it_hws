import React, { PureComponent } from 'react';
import * as icons from '../../icons';
import PropTypes from 'prop-types';
import './Icon.scss'

export default class Icon extends PureComponent {
    state = {
        filled: this.props.filled
    }

    componentDidUpdate() {
        this.props.updateIconState(this.state.filled);
    }

    toggleFavorite = () => {
        this.setState({filled: !this.props.filled});
        !this.state.filled ? 
        localStorage.setItem(`${this.props.productName} ${this.props.productColor}`, "Товар добавлен в избранное!") : 
        localStorage.removeItem(`${this.props.productName} ${this.props.productColor}`);
    }

    render() {
        const { filled } =this.state;
        const { type, iconColor } =this.props,
            iconJsx = icons[type];

        if (!iconJsx) {
            return null;
        }
        return (
            <div className="icon-star" onClick={this.toggleFavorite}>
                {iconJsx(iconColor, filled)}
            </div>
        );
    }
}

Icon.propTypes = {
    type: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    filled: PropTypes.bool.isRequired
}