import React, {Component} from 'react';
import Button from './../Button/Button';
import './Modal.scss';

class Modal extends Component {
    state = {
        modal: true
    }

    setInLocalStorage = () => {
        localStorage.setItem(`${this.props.productName} ${this.props.color}`, "Товар в корзине!");
        alert('Товар добавлен в корзину!');
        this.hideByInside();
    }

    hideByInside = () => {
        this.setState({ modal: false});
        this.props.updateModalState(!this.state.modal)       
    }

    hideByOutside = event => {
        const result = event.currentTarget === event.target ? this.props.updateModalState(!this.state.modal) && this.setState({ modal: false}) : null;
        return result;
        }

    render() {
        const {modal} = this.state;

        const { header, closeButton, text } = this.props;

        return (
            <>
            {modal && <div className="modal_wrapper popup-fade" onClick={this.hideByOutside}>
                <div className="modal">
                    <header className="modal_header">
                        <span>{header}</span><span className={closeButton} onClick={this.hideByInside}></span>
                    </header>
                    <p className="modal_text">{text}</p>
                    <div className="modal_footer">
                    <Button text="Добавить" className="modal-btn_submit" onClick={this.setInLocalStorage} />
                    <Button text="Отмена" className="modal-btn_cancel" onClick={this.hideByInside} />
                    </div>
                </div>
            </div>}
            </>
        );
    }
}

export default Modal;
