import React, { PureComponent } from 'react';
import Button from './../Button/Button';
import Modal from './../Modal/Modal';
import Icon from './../Icon/Icon';
import PropTypes from 'prop-types';
import './Product.scss'

export default class Product extends PureComponent {

state = {
        modal: false,
        filled: false
    }

updateModalState = (value) => {
    this.setState({ modal: value })
}

updateIconState = (value) => {
    this.setState({ filled: value })
}


render() {

        const { modal, filled } = this.state;
        const { productName, path, price, sku, color } =this.props;

        return (
            <div className="product product-card">
                <Icon type="star" iconColor="#ffc107" filled={filled} updateIconState={this.updateIconState} productName={productName} productColor={color}
                />
                <div className="img-wrapper">
                    <img className="product-img" src={path} alt={productName} />
                </div>
                <p className="product-sku"><span>Артикул:</span> {sku}</p>
                <p className="product-name">Экшн-камера {productName} {color}</p>
                <p className="product-price">{price} грн</p>
                <div>
                    <Button text="Add to cart" className="product-btn" onClick={() => {
                        this.setState({modal: !modal}) &&
                        this.props.updateModalState(this.state.modal)
                    }} />
                </div>
                               {modal && <Modal
                    productName={productName}
                    color={color}
                    header={`Добавить ${this.props.productName} ${this.props.color} в корзину?`}
                    closeButton="close"
                    updateModalState={this.updateModalState}
                    text="Наши менеджеры свяжутся с вами для подтверждения заказа и консультации." />}
        
            </div>
        )
    }
}

Product.propTypes = {
    productName: PropTypes.string.isRequired,
    path: PropTypes.string.isRequired,
    sku: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    updateModalState: PropTypes.func.isRequired
}

Product.defaultProps = {
    color: ""
}